<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




Route::get('/forget-password', 'Auth\ForgotPasswordController@gSendsPasswordResetEmails');
Route::post('/forget-password', 'Auth\ForgotPasswordController@postEmail');

Route::get('/home', 'HomeController@index');
Route::get('/error', 'C404Controller@index');

Auth::routes();


Route::get('register', 'Auth\RegisterController@create');
Route::get('login', 'Auth\LoginController@login');
