<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware(['auth:sanctum'])->group(function () {

//});


Route::middleware('auth:api')->group(function() {
Route::namespace('Api')->group(function() {
   Route::post('/logout', 'AuthController@logout');
  // Route::post('refresh', 'AuthController@refresh');
   Route::apiResource('admins','AdminsController');

    });
});


Route::namespace('Api')->group(function() {


    Route::post('login', 'UsersApiController@login');
    Route::post('register', 'UsersApiController@register');

    Route::post('sendrdv', 'AuthController@sendrdv');
    Route::post('sendform1', 'AuthController@sendform1');
    Route::get('email/verify/{id}','VerificationApiController@verify')->name('verificationapi.verify');
    Route::get('email/resend', 'VerificationApiController@resend')->name('verificationapi.resend');

   // Route::post(‘details’, ‘UsersApiController@details’)->middleware(‘verified’);
   // Route::post('/login', 'AuthController@login');
   // Route::post('/register','AuthController@register');

    Route::get('/verify/{token}', 'VerifyController@VerifyEmail');
    Route::post('/forgotpassword', 'AuthController@resetpassword');
    Route::post('/change_password/{id}', 'AuthController@change_password');

    Route::apiResource('posts', 'PostsController');
    Route::apiResource('services','ServicesController');
    Route::apiResource('products','ProductsController');
    Route::apiResource('pages','PagesController');
    Route::apiResource('users','UsersController');
    Route::apiResource('appointements','AppointementsController');

    Route::get('/sendmail', 'MailController@sendmail');
    Route::put('/user_avatar/{id}', 'UsersController@updateAvatar');
    Route::get('/posts/posts_user/{id_user}', 'PostsController@postsByUser');
    Route::get('/posts/posts_user_short/{id_user}', 'PostsController@postsByUserShort');
    });


