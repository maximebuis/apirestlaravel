<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Appointement extends Model
{
    protected $fillable = [
        'title', 'content','category', 'image', 'edited_by', 'view'
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'edited_by', 'id');
    }

    public function toArray(){
        $data = parent::toArray();
        $data['edited_by']=$this->user;
        $data['edited_by']->makeHidden('email_verified_at');
        $data['edited_by']->makeHidden('created_at');
        $data['edited_by']->makeHidden('updated_at');
        $data['edited_by']->makeHidden('email');
        return $data;
    }


     /** * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'start_at' => 'datetime',
        'end_at' => 'datetime',
    ];
}
