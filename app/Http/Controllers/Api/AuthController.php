<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\User;
use Illuminate\Support\Str;
use App\Mail\VerificationEmail;
use App\Mail\NewPassword;
use App\Mail\WelcomeMail;
use App\Mail\RdvEmail;
use App\Mail\Form1Mail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Hash;
class AuthController extends Controller


{

   public function login(Request $request){
    $request->validate([
        'email' => 'required|email|exists:users,email',
        'password' => 'required'
    ]);

    if( Auth::attempt(['email'=>$request->email, 'password'=>$request->password]) ) {
        $user = Auth::user();


    return response()->json($user);
    }else {


    return response()->json(['error'], 401);
    }}

    //  Register
    public function register(Request $request) {

    $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required',
            'firstname' => 'required|min:5',
            'lastname' => 'required|min:5',
        ]);

    if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
    }

    $user = User::create([
        'firstname' => $request->firstname,
        'lastname' => $request->lastname,
        'email' => $request->email,
        'password' => bcrypt($request->password),
        'remember_token' => Str::random(60)
    ]);

    Mail::to($user['email'])->send(new VerificationEmail($user));

    return response()->json([
        'message' => 'Successfully created',$user
    ]);

    }



    //Reset password
    public function resetpassword(Request $request){


    $validator = Validator::make($request->all(), [
        'email' => 'required|email',
        ]);

    if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
    }

    $input = $request->all();
    $newpassword =Str::random(10);
    $user = User::where('email',$request->email)->first();
    $user->password = Hash::make($newpassword);
    $user->save();


    $user = [
        'email' => $request->email,
        'password' => $newpassword,
    ];

    Mail::to($user['email'])->send(new NewPassword($user));
    return response()->json([
        'message' => 'Successfully password changed',$user
    ]);


    }

   //Update password

    public function change_password(Request $request,$id)
    {
        $validator = Validator::make($request->all(), [
            'new_password' => 'required|min:5',
            'confirm_password' => 'required|same:new_password',
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }
        $user = User::where('id',$id)->first();
        $user->password = Hash::make('new_password');
        $user->save();
        return response()->json([
            'message' => 'Successfully password changed'
        ]);
    }

        /**
     * Logout user (Revoke the token)
     *
     * @return [string] message
     */
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        return response()->json([
            'message' => 'Successfully logged out'
        ]);
    }

/**
     * details api
     *
     * @return \Illuminate\Http\Response
     */
    public function details()
    {
        $user = Auth::user();
        return response()->json(['success' => $user], $this-> successStatus);
    }



 //  Register
 public function sendrdv(Request $request) {



    $user = new user;
    $user->firstname = $request->firstname;
    $user->lastname = $request->lastname;
    $user->date = $request->date;
    $user->email = $request->email;
    $user->subject = "Demande de rendez-vous";
    $user->phone_number = $request->phone_number;
    $user->message = $request->message;
-

    Mail::to(env('ADMIN_EMAILS'))->send(new RdvEmail($user));

    return response()->json([
        'message' => 'Successfully created',$user
    ]);

    }


 //  Register
 public function sendform1(Request $request) {

    $user = new user;
    $user->firstname = $request->firstname;
    $user->lastname = $request->lastname;
    $user->email = $request->email;
    $user->subject = "Demande de rendez-vous";
    $user->message =  $request->message;

    Mail::to(env('ADMIN_EMAILS'))->send(new Form1Mail($user));

    return response()->json([
        'message' => 'Successfully created',$user
    ]);

    }


}


