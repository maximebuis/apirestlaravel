<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class VerifyController extends Controller
{
    public function VerifyEmail(Request $request)
    {
        $token = $request->token;

    	if($token == null) {
    		session()->flash('message', 'Invalid Login attempt');
    		return redirect()->route('login');
    	}
        $user = User::where('remember_token',$token)->first();

       if (!$user->hasVerifiedEmail()) {
        $user->markEmailAsVerified();
      } else {
        redirect()->route('error');
      }


    //  session()->flash('message', 'Your account is activated, you can log in now');
     return redirect()->route('home');

    }

}
