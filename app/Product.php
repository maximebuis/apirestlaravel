<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'title', 'content','category','ref','image','width','height','weight','view','edited_by'
    ];


}
