
@extends('layouts.app')

@section('content')

@include('layouts.headers.guest')

<div class="container-fluid mt--7">
    <div class="row">
        <div class="col">
            <div class="card shadow">
                <div class="card-header border-0">
                    <div class="row align-items-center">

                        <div class="col-8">
                            <h3 class="mb-0">Clients</h3>
                        </div>
                        <div class="col-4 text-right">
                            <a href="{{url('/appointments/create')}}" class="btn btn-sm btn-primary">Add rdv</a>
                        </div>
                    </div>
                </div>

                <div class="col-12">
                    <form action = "{{ route('users') }}" method = "GET" role = "search">
                        <div class="input-group mb-3">
                        {{ Form::text("search", old("search") ? old("search") : (!empty($search) ? $search : null), ["class" => "form-control", "placeholder" => "Rechercher des utilisateurs", "required", "oninput" => "this.form.submit()", "aria-describedby" => "button-addon2"])}}
                            @if(!empty($search))
                            <div class="input-group-append">
                                <button class="btn btn-outline-primary" type="reset" id="button-addon2" onclick="javascript: window.location = '{{ url('/users')}}';">Reset</button>
                            </div>
                            @endif
                        </div>
                        
                    <form>
                </div>

                @if(count($users) >=1)
                <div class="col-12">
                    <div class="table-responsive">
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col"></th>
                                    <th scope="col">ID</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Email</th>
                                    <th scope="col">Creation Date</th>
                                    <th scope="col"></th>
                                </tr>
                            </thead>
                             <tbody>
                             @foreach($users as $user)
                              <tr id="user{{$user->id}}" class="active">
                                    <td><img src="{{ asset('argon') }}/img/theme/team-4-800x800.jpg" width="40px" class="rounded-circle"></td>
                                    <td>{{ $user->id}}</td>
                                    <td>{{ $user->firstname}} {{ $user->lastname}}</td>
                                    <td>{{ $user->email}}</td>
                                    <td>{{ Carbon\Carbon::parse($user->created_at)->format('d-m-Y')}}</td>
                                    <td class="text-right">
                                        <div class="dropdown">
                                            <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="fas fa-ellipsis-v"></i>
                                            </a>
                                            <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                                <a class="dropdown-item" href="{{url('/users/edit')}}/{{$user->id}}">Edit</a>
                                                <a class="dropdown-item" data-toggle="modal" data-target="#DeleteModal" onclick="deleteData({{$user->id}})">Delete</a>

                                            </div>
                                        </div>
                                    </td>
                                </tr>
             
                                                                <!-- Modal -->
                                <div class="modal fade" id="DeleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                        <div class="modal-content">
                                            <form id="deleteForm" action="" method="GET" action="{{ route('users.delete', $user)}}">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">Supprimer</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    Voulez-vous vraiment supprimer ?
                                                </div>

                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
                                                    <button type="button" class="btn btn-primary"  type="button"  onclick="formSubmit()">Oui</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div> 
                                                                <!-- Modal -->
                            
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                @else
                <div class="col-12">
                    <p> Aucun utilisateur ne correspond à votre recherche </p>
                </div>
                @endif

                <div class="card-footer py-4">
                    <nav class="d-flex justify-content-end" aria-label="...">
                    {{ $users->links()}}
                    </nav>
                </div>
            </div>
        </div>
    </div>
    @include('layouts.footers.auth')
</div>
@endsection

<script type="text/javascript">
     function deleteData(id)
     {
         $id = id;
         console.log(id);
         var url = '{{ route("users.delete", ":id") }}';
         url = url.replace(':id',$id);
         $("#deleteForm").attr('action', url);
     }



     function formSubmit()
     {
         console.log("Form submit")
         $("#deleteForm").submit();
     }



  </script>
