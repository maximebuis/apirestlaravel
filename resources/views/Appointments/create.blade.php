@extends('layouts.app')
@section('content')
@include('layouts.headers.guest')

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-xl-8 order-xl-1">
                <div class="card bg-secondary shadow">
                    <div class="card-header bg-white border-0">
                        <div class="row align-items-center">
                            <h3 class="col-12 mb-0">{{ __('Add Appointment') }}</h3>
                        </div>



<div class="container mt-5">
    <div class="row">
        <div class="col-12 text-center"><h2>Laravel Autocomplete Search Using Typeahead JS - w3alert.com</h2></div>
        <div class="col-12 text-center">
            <div id="custom-search-input">
                <div class="input-group">
                    <input id="search" name="search" type="text" class="form-control" placeholder="Search" />
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">




    var route = "{{ url('autocomplete') }}";
    $('#search').typeahead({


        source:  function (term, process) {

            con
        return $.get(route, { term: term }, function (data) {
                return process(data);
            });
        }
    });
</script>   
         <div class="container" style="margin-top: 50px;">
            <div class="row">
                <div class="col-lg-3"></div>
                <div class="col-lg-6">
                    <h4 class="text-center">Autocomplete Search Box with <br> Laravel + Ajax + jQuery</h4><hr>
                    <div class="form-group">
                        <label>Type a country name</label>
                        <input type="text" name="country" id="country"  onclick="deleteData({{$page->id}})" placeholder="Enter country name" class="form-control">
                    </div>
                    <div id="country_list"></div>                    
                </div>
                <div class="col-lg-3"></div>
            </div>
        </div>
                    </div>
                    <div class="card-body">
                       
                    </div>
                </div>
            </div>
        </div>
        @include('layouts.footers.auth')
    </div>
@endsection

    <script type="text/javascript">



   function deleteData(id)
     {
         $id = id;
         console.log(id);
         var url = '{{ route("pages.destroy", ":id") }}';
         url = url.replace(':id',$id);
         $("#deleteForm").attr('action', url);
     }





            $(document).ready(function () {


                   function deleteData(id)
                           
                          console.log("frzg");
                $('#country').on('keyup',function() {
                    var query = $(this).val(); 
                    $.ajax({
                       
                        url:"{{ route('search') }}",
                  
                        type:"GET",
                       
                        data:{'country':query},
                       
                        success:function (data) {
                          
                          console.log(data);
                            $('#country_list').html(data);
                        }
                    })
                    // end of ajax call
                });

                
                $(document).on('click', 'li', function(){
                  
                    var value = $(this).text();
                    $('#country').val(value);
                    $('#country_list').html("");
                });
            });
        </script>