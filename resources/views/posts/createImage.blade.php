@extends('layouts.app', ['title' => __('User Profile')])

@section('content')
    @include('users.partials.header', [
        'title' => __(' '),
    ])

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-xl-8 order-xl-1">
                <div class="card bg-secondary shadow">
                    <div class="card-header bg-white border-0">
                        <div class="row align-items-center">
                            <h3 class="col-12 mb-0">{{ __('Create Post') }}</h3>
                        </div>
                    </div>
                    <div class="card-body">
                        <label class="form-control-label" for="image">{{ __('Image') }}</label>
                        <div class="panel-body">
                            <div class = "row">
                                <img src="{{url('http://127.0.0.1:8000/images/default.png')}}" width="400px" >

                                @if ($message = Session::get('success'))
                                <div class="alert alert-success alert-block">
                                    <button type="button" class="close" data-dismiss="alert">×</button>
                                        <strong>{{ $message }}</strong>
                                </div>
                                @endif

                                @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <strong>Whoops!</strong> There were some problems with your input.
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                                @endif

                                <div>
                                    <br/>
                                </div>
                                <form action="{{ route('image.upload.post', $post->id) }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                    <div class="row">
                                        <div class="col-md-8">
                                            <input type="file" name="image" class="form-control" id="image">
                                        </div>

                                        <div class="col-md-4">
                                            <button type="submit" class="btn btn-success">Upload</button>
                                        </div>

                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('layouts.footers.auth')
    </div>
@endsection
