
</html>
<!DOCTYPE html>
<html>
  <head>
    <title>Welcome Email</title>
  </head>
  <body>
  <h2>Bonjour {{$user['firstname']}}  {{$user['lastname']}}</h2>
    <br/>

    Merci pour votre inscription.
    Your registered email-id is {{$user['email']}} , Please click on the below link to verify your email account

    <a href="{{url('/verify', $user['remember_token'])}}">Verify Email</a>
    <br/>

  </body>
</html>
