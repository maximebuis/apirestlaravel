<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
       // $this->call([UsersTableSeeder::class]);
        factory(App\User::class, 1)->create();
        factory(App\Post::class, 11)->create();
        factory(App\Product::class, 11)->create();
        factory(App\Page::class, 5)->create();
        factory(App\Appointement::class, 11)->create();
        factory(App\Service::class, 11)->create();

    }
}
