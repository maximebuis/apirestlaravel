<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Product;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Product::class, function (Faker $faker) {
    return [
        'title' => $faker->realText(25),
        'content'=> $faker->realText(),
        'category'=> $faker->realText(20),
         'ref'=> rand(1, 50),
         'width' => rand(1, 50),
         'height' => rand(1, 50),
         'weight' => rand(1, 50),
         'view' => rand(0, 1),
         'edited_by'=> rand(1, 50),
         'image' => $faker->image('public/storage/images',640,480, null, false)
    ];
});
