<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Gallery;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Gallery::class, function (Faker $faker) {
    return [
        'title' => $faker->realText(25),
        'content'=> $faker->realText(10),
        'category'=> $faker->realText(20),
        'view' => 1,
        'edited_by'=> 1,
        'image' => $faker->image('public/storage/images',640,480, null, false)
    ];
});
