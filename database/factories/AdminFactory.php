<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Admin;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Admin::class, function (Faker $faker) {
    return [
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'firstname' => $faker->firstName,
        'lastname' => $faker->lastName,
        'password' => 'password',
        'remember_token' => Str::random(10),
    ];
});
